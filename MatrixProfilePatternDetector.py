import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import stumpy
import pickle
from scipy import signal
from dask.distributed import Client
dask_client = Client()

class MatrixProfilePatternDetector (object):
    
    def __init__(self, timeseries):
        """
        timeseries - Timeseries data
        """
        self.T = timeseries
        
    def calculate_matrix_profile(self, window_size):
        """
        window_size - length of patterns
        """
        self.window_size = window_size
        self.mp = stumpy.stumped(dask_client, self.T, m = window_size)
        print("Min value of euclidian distance - ", np.min(self.mp[:,0]))
        print("Median value of euclidian distance - ", np.median(self.mp[:,0]))
        print("Max value of euclidian distance - ", np.max(self.mp[:,0]))
        
    def get_patterns(self, threshold, reduction_ratio, mode = "normal"):
        """
        threshold - threshold of euclidian distance which is considered the border of the pattern
        reduction_ratio - ratio for reduction of threshold
        mode - normal (works longer but the result is large)
        mode - strict (works fast but the result is more accurate)
        """
        self.patterns = self.get_all_patterns(self.mp, threshold, reduction_ratio, mode)
        
    def get_simillar(self, mp, i, threshold, reduction_ratio, mode):
        """
        mp - Matrix profile
        i - index of pattern
        threshold - threshold of euclidian distance which is considered the border of the pattern
        reduction_ratio - ratio for reduction of threshold
        """
        queue = set([i])
        queue_threshold = [threshold]
        indexes = set()
        while len(queue):
            el = queue.pop()
            t = queue_threshold.pop()
            if el in indexes:
                continue
            if t and mp[mp[el][1]][0] > t: continue
            if el==-1 or el is None: continue

            if mode != "strict":
                if el!=mp[el][3]:
                    indexes.add(el)
                    queue.add(mp[el][3])
                    queue_threshold.append(t * reduction_ratio)

            if el!=mp[el][1]:
                indexes.add(el)
                queue.add(mp[el][1])
                queue_threshold.append(t * reduction_ratio)
        if len(indexes) < 3: return None
        return indexes

    def get_all_patterns(self, mp, threshold = None, reduction_ratio = 1, mode = "normal"):
        """
        mp - Matrix profile
        i - index of pattern
        threshold - threshold of euclidian distance which is considered the border of the pattern
        reduction_ratio - ratio for reduction of threshold
        """
        patterns = {}
        cycle = np.where(mp[:,0] < threshold)[0]
        for i in cycle:
            pattern = self.get_simillar(mp, i, threshold, reduction_ratio, mode)
            if pattern:
                max_el = max(pattern)
                if max_el in patterns:
                    patterns[max_el] = patterns[max_el].union(pattern)
                else:
                    patterns[max_el] = pattern

        patterns_list = []
        for i in patterns:
            patterns_list.append(list(patterns[i]))
        patterns_list = sorted (patterns_list, key = len, reverse = True)
        return patterns_list

    def plot_patterns(self, patterns, max_quantity = 5, fig_size = (15,15)): 
        """ 
        T - timeseries,
        patterns - patterns,
        window_size - length of pattern,
        max_quantity - max quantity of patterns for draw
        """
        c = 1
        plt.figure(figsize = fig_size)
        for i in range(len(patterns)):  
            for j in range(max_quantity):
                try:
                    plt.subplot(len(patterns), max_quantity, c)
                    plt.title(str(i) + " - " + str(patterns[i][j]))
                    plt.plot(self.T[patterns[i][j] : patterns[i][j] + self.window_size])
                except:
                    pass
                c+=1

    def mark_up_patterns(self, patterns, pass_indexes = 1, zoom = None):
        """
        T - timeseries,
        patterns - patterns,
        window_size - length of pattern,
        pass_indexes - number of skipped patterns to speed up drawing,
        zoom - zoom
        """
        t = pd.Series(self.T)
        if zoom:
            for i in range(len(patterns)):
                plt.figure(figsize = (16,6))
                plt.title("Zoomed" + str(i+1))
                t[zoom[0] : zoom[1]].plot()
                prev = -1000000
                patterns[i] = sorted(patterns[i])
                for j in patterns[i]:
                    if abs(j - prev) >= self.window_size:
                        if j >= zoom[0] and j + self.window_size <= zoom[1]:
                            t[j : j + self.window_size].plot(color = "r")
                            plt.scatter([j, j + self.window_size - 1], [t[j], t[j + self.window_size-1]], c = "black")
                            prev = j
        else:
            for i in range(len(patterns)):
                plt.figure(figsize = (16,6))
                plt.title("Pattern " + str(i + 1))
                t.plot()
                for j in patterns[i][::pass_indexes]:
                    t[j:j+self.window_size].plot(color = "r")
                    plt.scatter([j, j + self.window_size], [t[j], t[j + self.window_size-1]], c = "black")